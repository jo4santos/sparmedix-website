<?php

$js_form_action = "";
if(isset($_POST["js_form_action"])) {
	$js_form_action = $_POST["js_form_action"];
}
$js_form_ajax = "";
if(isset($_GET["js_form_ajax"])) {
	$js_form_ajax = $_GET["js_form_ajax"];
}


switch ($js_form_action) {
	case "send_email":
	{		
		$js_verification_emp = isset($_POST["js_verification_emp"])?$_POST["js_verification_emp"]:"";
		$js_verification_val = isset($_POST["js_verification_val"])?$_POST["js_verification_val"]:"";
		
		if($js_verification_emp != "" || $js_verification_val != "js_default")
		{
			echo json_encode(array("type"=>"error","message"=>"Ocorreu um erro ao enviar a mensagem, tente novamente."));
			exit();
		}
		
		$name = isset($_POST["name"])?$_POST["name"]:"";
		$email = isset($_POST["email"])?$_POST["email"]:"";
		$subject = isset($_POST["subject"])?$_POST["subject"]:"";
		$message = isset($_POST["message"])?$_POST["message"]:"";

		if($name.$email.$subject.$message=="") {
			echo json_encode(array("type"=>"error","message"=>"Preencha todos os campos obrigatórios e tente novamente."));
			exit();			
		}

		$phone = isset($_POST["phone"])?$_POST["phone"]:"";
		
		$content = "";
		$content .= "Caro administrador, foi recebida a seguinte mensagem através do website sparmedix.pt:<br>\n<br>\n-----------------------------------";
		$content .= "<br>\n<b>Nome: </b> ".$name;
		$content .= "<br>\n<b>Email: </b> ".$email;
		$content .= "<br>\n<b>Telefone: </b> ".$phone;
		$content .= "<br>\n<b>Assunto: </b> ".$subject;
		$content .= "<br>\n<b>Mensagem:</b><br>\n".$message;
		$content .= "<br>\n-----------------------------------<br>\n<br>\n---<br>\nPor favor não responda a este e-mail, foi automaticamente gerado pela página web da SPARMEDIX. Para mais informações envie um e-mail para js@josesantos.eu";
		
		$headers = "From: " . strip_tags($email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";		
		$headers .= "X-Mailer: php\r\n";
		
	 	if (!mail("jo4santos@gmail.com", "[WEB] ".$subject, $content, $headers))
	 	{
			echo json_encode(array("type"=>"error","message"=>"Ocorreu um erro ao enviar a mensagem, tente novamente."));
			exit();
		}
		
	 	if (!mail("geral@sparmedix.pt", "[WEB] ".$subject, $content, $headers))
	 	{
			echo json_encode(array("type"=>"error","message"=>"Ocorreu um erro ao enviar a mensagem, tente novamente."));
			exit();
		}

		echo json_encode(array("type"=>"success","message"=>"Mensagem enviada, responderemos logo que possível. Obrigado."));
		exit();
	}
}
?>