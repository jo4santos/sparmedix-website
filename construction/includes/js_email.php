<?php

if(!function_exists("js_send_email"))
{
	function js_send_email($destination_email, $origin_email, $origin_name, $subject, $content)
	{
		/*$content .= JS_MAIL_CONTENT_SUFFIX;
		
		$transport = Swift_SmtpTransport::newInstance('mail.josesantos.eu', 25)
			->setUsername('js@josesantos.eu')
			->setPassword('qwe123ASD$%&');
		
		$mailer = Swift_Mailer::newInstance($transport);
		
		$message = Swift_Message::newInstance()
		->setSubject(JS_MAIL_SUBJECT_PREFIX.$subject)
		->setFrom(array($origin_email => $origin_name))
		->setTo(array($destination_email))
		->setBody(strip_tags($content))
		->addPart($content, 'text/html')
		;
		
		if ($mailer->send($message))
		{
			error_log("Message sent!");
			return true;
		}
		else
		{
			error_log("Message delivery failed...");
			return false;
		}*/
		
		// validate all fields. Check for email headers! BCC for example. Empty strings
		
		$headers = "From: " . strip_tags($origin_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($origin_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";		
		$headers .= "X-Mailer: php\r\n";
		
		$message = $content.JS_MAIL_CONTENT_SUFFIX;
		
	 	if (mail($destination_email, JS_MAIL_SUBJECT_PREFIX.$subject, $message, $headers))
	 	{
			error_log("Message sent!");
			return true;
		} 
		else 
		{
			error_log("Message delivery failed...");
			return false;
		}
	}
}
?>