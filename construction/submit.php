<?php

$js_form_action = "";
if(isset($_POST["js_form_action"])) {
	$js_form_action = $_POST["js_form_action"];
}
$js_form_ajax = "";
if(isset($_GET["js_form_ajax"])) {
	$js_form_ajax = $_GET["js_form_ajax"];
}


switch ($js_form_action) {
	case "insert_email":
	{
		session_start();
		
		$js_verification_emp = isset($_POST["js_verification_emp"])?$_POST["js_verification_emp"]:"";
		$js_verification_val = isset($_POST["js_verification_val"])?$_POST["js_verification_val"]:"";
		
		if($js_verification_emp != "" || $js_verification_val != "js_default")
		{
			$_SESSION["js_error"] = "Ocorreu um erro ao introduzir o e-mail, tente novamente.";
			header("location: ".$_SERVER["HTTP_REFERER"]);
			exit();
		}
		
		$js_email_input = isset($_POST["js_email_input"])?$_POST["js_email_input"]:"";
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			$js_user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$js_user_ip = $_SERVER['REMOTE_ADDR'];
		
		$js_user_ip = trim(end(explode(",",$js_user_ip)));
		
		$source = "emails.xml";
		$entries = new SimpleXMLElement($source,null,true);
		
		$ip_counter = 0;
		foreach ($entries as $entry)
		{
			if((string)$entry->email == $js_email_input)
			{
				$_SESSION["js_warning"] = "O seu e-mail já se encontra registado na nossa lista, obrigado.";
				header("location: ".$_SERVER["HTTP_REFERER"]);
				exit();
			}
			if((string)$entry->ip == $js_user_ip)
			{
				$ip_counter++;
				if($ip_counter>=5)
				{
					$_SESSION["js_error"] = "Limite de 5 registos por IP atingido, o endereço não foi registado.";
					header("location: ".$_SERVER["HTTP_REFERER"]);
					exit();
				}
			}
		}
		
		$entry = $entries->addChild('entry');
		$entry->addChild('email',$js_email_input);
		$entry->addChild('ip',$js_user_ip);
		
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($entries->asXML());
		$dom->save('emails.xml');
		
		define(	'JS_MAIL_SUBJECT_PREFIX', 	"[SPARMEDIX] ");
		define(	'JS_MAIL_CONTENT_SUFFIX', 	"<br>\n<br>\n---<br>\nPor favor não responda a este e-mail, foi automaticamente gerado pela página temporária da SPARMEDIX. Para mais informações envie um e-mail para js@josesantos.eu");
		include "includes/js_email.php";
		
		js_send_email("jo4santos@gmail.com", $js_email_input, "SPARMEDIX - página temporária", "E-mail para notificacao", "O seguinte e-mail foi registado para notificação de website concluído:<br><br>".$js_email_input);
		js_send_email("geral@sparmedix.pt", $js_email_input, "SPARMEDIX - página temporária", "E-mail para notificacao", "O seguinte e-mail foi registado para notificação de website concluído:<br><br>".$js_email_input);
		
		$_SESSION["js_success"] = "Endereço de e-mail registado com sucesso, obrigado.";
		header("location: ".$_SERVER["HTTP_REFERER"]);
		exit();
		break;
	}
}
?>