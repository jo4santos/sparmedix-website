<?php
	include 'includes/js_mobile_detect.php';
	session_start(); 
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=250px, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>SparMedix - Distribuição Farmacêutica, Lda</title>
<link rel="shortcut icon" href="favicon.ico?v=1.0" />
<link rel="apple-touch-icon" href="apple-touch-icon-precomposed.png" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif' rel='stylesheet' type='text/css'>
<link type='text/css' rel="stylesheet" href="js/qtip2/jquery.qtip.min.css" />
<link type='text/css' rel="stylesheet" href="styles/style.php" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.5&sensor=false"></script>
<script type="text/javascript" src="js/maps/vendor/markermanager.js"></script>
<script type="text/javascript" src="js/maps/vendor/StyledMarker.js"></script>
<script type="text/javascript" src="js/maps/vendor/jquery.metadata.js"></script>
<script type="text/javascript" src="js/maps/jquery.jmapping.js"></script>
<script type="text/javascript" src="js/qtip2/jquery.qtip.js"></script>
<script type="text/javascript" src="js/modernizr.custom.03681.js"></script>
<script>
var myMessages = ['info','warning','error','success']; // define the messages types		 
function hideAllMessages()
{
		 var messagesHeights = new Array(); // this array will store height for each
	 
		 for (i=0; i<myMessages.length; i++)
		 {
				  messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
				  $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport	  
		 }
}

function showMessage(type)
{
	$('.'+ type +'-trigger').click(function(){
		  hideAllMessages();				  
		  $('.'+type).animate({top:"0"}, 500);
	});
}
$('document').ready(function(){
if(Modernizr.svg)
{
	$(".js_logo").eq(0).attr("src","logo.svg");
}else{
	$(".js_logo").eq(0).attr("src","logo.png");
}
	$(".tooltip").qtip({
		style: {
			classes: 'ui-tooltip-tipsy ui-tooltip-shadow js_tooltip'
		},
		position: {
			my: "bottom center",
			at: "top center",
			viewport: $(window)
		}
	});
    $('.js_map_container').jMapping({
        map_config: {
          navigationControlOptions: {
            style: google.maps.NavigationControlStyle.DEFAULT
          },
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoom: 4,
          scrollwheel: false
        },
        default_zoom_level: 15,
        always_show_markers: true
      });
    function js_validate_email(email) {
    	var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    	return emailRegEx.test(email);
    }
    $('#js_email_input').focus(function() {
    	$("#js_email_input").qtip("hide");
		if($(this).val() == 'insira o seu e-mail...') {
			$(this).val('');
		}
	});
    if($('#js_email_input').val() != 'insira o seu e-mail...') {
    	$('#js_email_input').addClass("filled");
    }
	$('#js_email_input').blur(function() {
		if($(this).val() == '') {
			$(this).val('insira o seu e-mail...');
			$(this).removeClass("filled");
		}
		else
		{
			$(this).addClass("filled");
		}
	});
	$('a.js_tooltip_ajax').each(function()
	{
		$(this).qtip(
		{
			content: {
				text: '<img class="throbber" src="styles/js_loader.gif" alt="Loading..." />',
				ajax: {
					url: $(this).attr('rel')
				}
			},
			style: {
				classes: 'ui-tooltip-tipsy ui-tooltip-shadow'
			},
			position: {
				my: <?= (($js_terminal->isMobile())?"'center'":"'bottom left'") ?>,
				at: <?= (($js_terminal->isMobile())?"'center'":"'top right'") ?>,
				<?= (($js_terminal->isMobile())?"target: $(window),":"") ?>
				viewport: $(window),
				effect: false
			},
			show: {
				event: 'click',
				solo: true,
				modal: true
			},
			hide: 'unfocus',
			style: {
				classes: 'ui-tooltip-light js-ajax-tooltip'
			},
			events: {
				hide: function(event, api) {
					$(".selected_tooltip").remove();
				}
			}
		})
	})
	.click(function(event) { 
		event.preventDefault();
		
		$position = $(this).position();
		$new_el = $(this).clone();
		$(this).after($new_el);
		$new_el.css({"left":$position.left,"top":$position.top});
		$new_el.addClass("selected_tooltip");
	});
	$("#js_email_input").qtip(
	{
		content: {
			text: "<img src='styles/js_icon_warning.png' width='24' height='24' />Introduza um endereço de e-mail"
		},
		position: {
			my: "bottom center",
			at: "top center"
		},
		show: false,
		hide: false,
		style: {
			classes: 'ui-tooltip-light ui-tooltip-shadow js-validation-tooltip'
		}
	});
	$("#js_email_form").unbind('submit').submit(function(e){
		$input_value = $("#js_email_input").val();
		if($input_value == 'insira o seu e-mail...' || $input_value == '') 
		{
			$("#js_email_input").qtip('option','content.text', "<img src='styles/js_icon_warning.png' width='24' height'24' />Introduza um endereço de e-mail");
			$("#js_email_input").qtip("show");
			e.preventDefault();
		}
		else
		{
			if(!js_validate_email($input_value))
			{
				$("#js_email_input").qtip('option', 'content.text', "<img src='styles/js_icon_warning.png' width='24' height='24' />Endereço de e-mail inválido");
				$("#js_email_input").qtip("show");
				e.preventDefault();
			}
			else
			{
				$(this).submit();
			}
		}
		e.preventDefault();
	});
	$("a[href=#js_email_input]").click(function(){$("#js_email_input").focus();});
	$("#js_email_reset").hide().click(function(){
		$("#js_email_input").val('insira o seu e-mail...');
		$("#js_email_input").removeClass("filled");
		$("#js_email_input").qtip("hide");
		$("#js_email_reset").hide();
	});
	$("#js_email_input").keyup(function(e){
		if($("#js_email_input").val()!="")
		{
			$("#js_email_reset").show();
		}
		else
		{
			$("#js_email_reset").hide()
		}
	});
	$("#js_email_input").change(function(e){
		if($("#js_email_input").val()!="")
		{
			$("#js_email_reset").show();
		}
		else
		{
			$("#js_email_reset").hide()
		}
	});
	if($("#js_email_input").val()!="" && $("#js_email_input").val()!='insira o seu e-mail...')
	{
		$("#js_email_reset").show();
	}
	else
	{
		$("#js_email_reset").hide()
	}
	// Initially, hide them all
	 hideAllMessages();
	 
	 // When message is clicked, hide it
	 $('.js_message').animate({top: -$(this).outerHeight()}, 500);
	 
	 // When message is clicked, hide it
	 $('.js_message').click(function(){			  
			  $(this).animate({top: -$(this).outerHeight()}, 500);
	  });
});
</script>
</head>
<body>
<?php 
if(isset($_SESSION["js_success"]))
{
?>
<div class="success js_message">
	<?= $_SESSION["js_success"]?>
</div>
<?php 
unset($_SESSION["js_success"]);
}
?>
<?php 
if(isset($_SESSION["js_error"]))
{
?>
<div class="error js_message">
	<?= $_SESSION["js_error"]?>
</div>
<?php
unset($_SESSION["js_error"]); 
}
?>
<?php 
if(isset($_SESSION["js_warning"]))
{
?>
<div class="warning js_message">
	<?= $_SESSION["js_warning"]?>
</div>
<?php
unset($_SESSION["js_warning"]); 
}
?>
<div id="body_wrapper">
	<img class="js_logo" src="logo.svg" alt="Logo" width="300px" />
	<div class="js_main_container">
		<h1>
			Este espaço encontra-se em remodelação. <span class="js_color_text"><br>Até breve!</span>
		</h1>
		<div class="js_description">
			<!-- Estamos a trabalhar para lhe oferecer todas as informações que necessita sobre a <a href="#about_us" title="A nossa empresa" rel="pages/about_us.html" class="js_tooltip_ajax"><b>nossa empresa</b></a>, com conteúdos sempre atualizados e disponíveis de forma simples e apelativa. Agradecemos que <a href="#js_email_input"><b>deixe o seu e-mail</b></a> para que possamos informá-lo quando o novo website estiver concluído. Não hesite em contactar-nos, os <a href="#services" rel="pages/services.html" title="Os nossos serviços" class="js_tooltip_ajax"><b>nossos serviços</b></a> estão ao seu dispôr!  -->
			Estamos a trabalhar para lhe oferecer todas as informações que necessita sobre a nossa empresa, com conteúdos sempre atualizados e disponíveis de forma simples e apelativa. Agradecemos que <a href="#js_email_input"><b>deixe o seu e-mail</b></a> para que possamos informá-lo quando o novo website estiver concluído. Não hesite em contactar-nos, os nossos serviços estão ao seu dispôr!
		</div>
	</div>
	<div class="js_contact_container">
		<div class="js_contact_form">
			<form action="submit.php" method="post" id="js_email_form">
				<input type="hidden" id="js_form_action" name="js_form_action" value="insert_email" />
				<input type="hidden" id="js_verification_emp" name="js_verification_emp" value="" />
				<input type="hidden" id="js_verification_val" name="js_verification_val" value="js_default" />
				<div>
					<input type="text" id="js_email_input" class="js_email_input" name="js_email_input" value="insira o seu e-mail..." />
					<input type="reset" id="js_email_reset" class="js_email_reset" name="js_email_reset" value="" />
					<span class="js_contact_form_comment js_normal js_color_text">Nós não enviamos spam.</span>
				</div>
				<input type="submit"
					id="js_email_submit" class="js_email_submit" value="Submeter" />
					<span class="js_contact_form_comment js_mobile js_color_text">Nós não enviamos spam.</span>
			</form>
		</div>
		<div class="js_contact_info">
			<span class="js_color_text">Telefone: </span><br><a style="margin-left:10px;" href="tel:223389029">(+351) 22 338 90 29</a><br>
			<span class="js_color_text">Fax: </span><br><a style="margin-left:10px;" href="fax:223389032">(+351) 22 338 90 32</a><br>
			<span class="js_color_text">Email: </span><br><a style="margin-left:10px;" href="mailto:geral@sparmedix.pt">geral@sparmedix.pt</a><br>
			<span class="js_color_text">Morada: </span><br><a style="margin-left:10px;display: block;" href="https://mapsengine.google.com/map/edit?mid=zgKH4MxGCBKA.kHyH7XJg4WzA" target="_blank">Rua Delfim Ferreira 323,<br>4100-201 Porto</a>
		</div>
	</div>
	<div id="map-side-bar">
		<div class="map-location"
			data-jmapping="{id: 1, point: {lng: -8.649661, lat: 41.171777}, category: 'office'}">
			<img src="styles/js_icon_maps.png" style="margin: 0px 10px 0px 0px; padding-bottom: 2px;" /><a href="#" class="map-link ml-10">Os nossos escritórios</a>

			<div class="info-box">
				<p>
					<img class="js_logo" src="logo.svg" alt="Logo" width="250px" style="margin: 0px; padding: 0px;" />
				</p>
			</div>
		</div>
	</div>
	<div class="js_map_container"></div>
	<div class="js_footer_container">
		&copy 2012 Desenvolvido por
		<a href="http://www.josesantos.eu" target="_blank" class="tooltip" title="Desenvolvimento de soluções web" style="display: inline-block;">
			<img style="height:30px;vertical-align:middle; margin-left: 5px;border:none;" height="30" width="120" src="js_logo_small_120x30.png" />
		</a>
	</div>
	</div>
</body>
</html>
