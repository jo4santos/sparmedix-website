<?php 
	class js_colors {
		function __construct($args=array()) {
			$arg_main = str_replace(" ","",$args["main"]);
			if(substr($arg_main, 0,3) == "rgb")
			{
				$rgb = str_replace(array("rgb","(",")"," "), "", $arg_main);
				$rgb = explode(",",$rgb);
			}
			elseif ($arg_main[0] == "#")
			{
				$hex = str_replace("#","",$arg_main);
				$rgb = str_split($hex, 2);
				$rgb[0]=hexdec($rgb[0]);$rgb[1]=hexdec($rgb[1]);$rgb[2]=hexdec($rgb[2]);
			}			
			
			$d_light = 15;
			$c_light = array("r"=> max(0,min(255,$rgb[0]+$d_light)),"g"=>max(0,min(255,$rgb[1]+$d_light)),"b"=> max(0,min(255, $rgb[2]+$d_light)));
			
			$d_lighter = 30;
			$c_lighter = array("r"=> max(0,min(255,$rgb[0]+$d_lighter)),"g"=>max(0,min(255,$rgb[1]+$d_lighter)),"b"=> max(0,min(255, $rgb[2]+$d_lighter)));
			
			$d_dark = -20;
			$c_dark = array("r"=> max(0,min(255,$rgb[0]+$d_dark)),"g"=>max(0,min(255,$rgb[1]+$d_dark)),"b"=> max(0,min(255, $rgb[2]+$d_dark)));
			
			$d_darker = -30;
			$c_darker = array("r"=> max(0,min(255,$rgb[0]+$d_darker)),"g"=>max(0,min(255,$rgb[1]+$d_darker)),"b"=> max(0,min(255, $rgb[2]+$d_darker)));
			
			$d_shadow 	= -50;
			$c_shadow = array("r"=> max(0,min(255,$rgb[0]+$d_shadow)),"g"=>max(0,min(255,$rgb[1]+$d_shadow)),"b"=> max(0,min(255, $rgb[2]+$d_shadow)));
			
			$this->{"main"} 	= "rgb(".$rgb[0].",".$rgb[1].",".$rgb[2].")";
			$this->{"light"} 	= "rgb(".$c_light["r"].",".$c_light["g"].",".$c_light["b"].")";
			$this->{"lighter"} 	= "rgb(".$c_lighter["r"].",".$c_lighter["g"].",".$c_lighter["b"].")";
			$this->{"dark"} 	= "rgb(".$c_dark["r"].",".$c_dark["g"].",".$c_dark["b"].")";
			$this->{"darker"} 	= "rgb(".$c_darker["r"].",".$c_darker["g"].",".$c_darker["b"].")";
			$this->{"shadow"} 	= "rgb(".$c_shadow["r"].",".$c_shadow["g"].",".$c_shadow["b"].")";
			
			$this->{"main_hex"} 	= $this->RGBtoHex($rgb[0],$rgb[1],$rgb[2]);
			$this->{"light_hex"} 	= $this->RGBtoHex($c_light["r"],$c_light["g"],$c_light["b"]);
			$this->{"lighter_hex"} 	= $this->RGBtoHex($c_lighter["r"],$c_lighter["g"],$c_lighter["b"]);
			$this->{"dark_hex"} 	= $this->RGBtoHex($c_dark["r"],$c_dark["g"],$c_dark["b"]);
			$this->{"darker_hex"} 	= $this->RGBtoHex($c_darker["r"],$c_darker["g"],$c_darker["b"]);
			$this->{"shadow_hex"} 	= $this->RGBtoHex($c_shadow["r"],$c_shadow["g"],$c_shadow["b"]);
			
			$this->{"js_btns"} = array();			
			foreach ($args["buttons"] as $arg)
			{
					$arg_main = str_replace(" ","",$arg["color"]);
					if(substr($arg_main, 0,3) == "rgb")
					{
						$rgb = str_replace(array("rgb","(",")"," "), "", $arg_main);
						$rgb = explode(",",$rgb);
					}
					elseif ($arg_main[0] == "#")
					{
						$hex = str_replace("#","",$arg_main);
						$rgb = str_split($hex, 2);
						$rgb[0]=hexdec($rgb[0]);$rgb[1]=hexdec($rgb[1]);$rgb[2]=hexdec($rgb[2]);
					}
						
					$d_light = 20;
					$c_light = array("r"=> max(0,min(255,$rgb[0]+$d_light)),"g"=>max(0,min(255,$rgb[1]+$d_light)),"b"=> max(0,min(255, $rgb[2]+$d_light)));

					$d_lighter = 10;
					$c_lighter = array("r"=> max(0,min(255,$rgb[0]+$d_lighter)),"g"=>max(0,min(255,$rgb[1]+$d_lighter)),"b"=> max(0,min(255, $rgb[2]+$d_lighter)));
					
					$d_dark = -30;
					$c_dark = array("r"=> max(0,min(255,$rgb[0]+$d_dark)),"g"=>max(0,min(255,$rgb[1]+$d_dark)),"b"=> max(0,min(255, $rgb[2]+$d_dark)));

					$d_darker = -60;
					$c_darker = array("r"=> max(0,min(255,$rgb[0]+$d_darker)),"g"=>max(0,min(255,$rgb[1]+$d_darker)),"b"=> max(0,min(255, $rgb[2]+$d_darker)));
					
					$d_shadow 	= -120;
					$c_shadow = array("r"=> max(0,min(255,$rgb[0]+$d_shadow)),"g"=>max(0,min(255,$rgb[1]+$d_shadow)),"b"=> max(0,min(255, $rgb[2]+$d_shadow)));
						
					$main 		= "rgb(".$rgb[0].",".$rgb[1].",".$rgb[2].")";
					$light 		= "rgb(".$c_light["r"].",".$c_light["g"].",".$c_light["b"].")";
					$lighter 	= "rgb(".$c_lighter["r"].",".$c_lighter["g"].",".$c_lighter["b"].")";
					$dark 		= "rgb(".$c_dark["r"].",".$c_dark["g"].",".$c_dark["b"].")";
					$darker 	= "rgb(".$c_darker["r"].",".$c_darker["g"].",".$c_darker["b"].")";
					$shadow		= "rgb(".$c_shadow["r"].",".$c_shadow["g"].",".$c_shadow["b"].")";
						
					$main_hex 		= $this->RGBtoHex($rgb[0],$rgb[1],$rgb[2]);
					$light_hex 		= $this->RGBtoHex($c_light["r"],$c_light["g"],$c_light["b"]);
					$lighter_hex 	= $this->RGBtoHex($c_lighter["r"],$c_lighter["g"],$c_lighter["b"]);
					$dark_hex 		= $this->RGBtoHex($c_dark["r"],$c_dark["g"],$c_dark["b"]);
					$darker_hex		= $this->RGBtoHex($c_darker["r"],$c_darker["g"],$c_darker["b"]);
					$shadow_hex		= $this->RGBtoHex($c_shadow["r"],$c_shadow["g"],$c_shadow["b"]);
					
					array_push($this->{"js_btns"}, 
						array(	
							"class"			=>	$arg["class"],
							"main"			=>	$main,
							"light"			=>	$light,
							"lighter"		=>	$lighter,
							"dark"			=>	$dark,
							"darker"		=>	$darker,
							"shadow"		=>	$shadow,
							"main_hex"		=>	$main_hex,
							"light_hex"		=>	$light_hex,
							"lighter_hex"	=>	$lighter_hex,
							"dark_hex"		=>	$dark_hex,
							"darker_hex"	=>	$darker_hex,
							"shadow_hex"	=>	$shadow_hex
							)
					);
				
			}
		}
		function RGBtoHex($r,$g,$b) {
			return "#".str_pad(dechex($r), 2, '0', STR_PAD_LEFT).str_pad(dechex($g), 2, '0', STR_PAD_LEFT).str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
		}
		function HextoRGB($h) {
			$h = str_replace("#","",$h);
			$rgb = str_split($h, 2);
			$rgb[0] = hexdec($rgb[0]);
			$rgb[1] = hexdec($rgb[1]);
			$rgb[2] = hexdec($rgb[2]);			
			return "rgb(".$rgb[0].",".$rgb[1].",".$rgb[2].")";
		}
	}
?>	