.js_mobile {
	display: inline-block;
}
.js_normal {
	display: none;
}
html {
	width: 320px;
	margin: auto;
}
h1 {
	font-family: 'Droid Serif', serif;
	display: block;
	font-size: 1.4em;
	line-height: 1em;
	font-weight: normal;
	
	margin: 10px 0px;
}
.js_description {
	line-height: 1.4em;
	font-size: 0.85em;
	max-width: 700px;
	
	margin: 40px 0px;
}
#js_email_input {
	margin: 0px;
	padding-left: 15px;
	padding-right: 35px;
}
#js_email_reset {
	top: 15px;
	left: 260px;
}
#js_email_submit {
	width: 291px;
	margin: 0px;
}
.js_contact_info {
	font-size: 1.2em;
	padding-left: 15px;
	border-left: solid 5px <?= $color->light; ?>;
	margin: 15px 0px;
}
.js_contact_form_comment {
	margin-top: 10px;
}
.js-ui-tooltip {
	margin-left: 0px;
}
.js-ui-tooltip .ui-tooltip-content {
	font-size: 1.2em;
	line-height: 1.4em;
	padding: 10px 10px;
	min-width: 250px;
	max-width: 800px;
}