<?php 
	header("Content-type: text/css"); 	
	/*
	 * 	Examples
	*/
	$blue 	= "#087ab9";
	$red 	= "rgb(219, 2, 1)";
	$green 	= "rgb(50, 140, 50)";
	$orange	= "#EE6E0E";
	$grey	= "#888888";
	
	$main = $blue;
	
	include("js_css_scripts/js_colors.php");
	$color = new js_colors(
		array(
			"main" 			=> 	$main,
			"buttons"		=>	array(
				array("class" => "normal"	,"color" => $main)
			)
		)
	);
	$btn = $color->js_btns[0];
	include("style_main.php");
	include '../includes/js_mobile_detect.php';
	global $js_terminal;
	if($js_terminal->isMobile())
	{
		include("style_mobile.php");
	}
?>