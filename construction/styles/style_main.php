* {
	margin: 0px;
	padding: 0px;
}
.js_color_text {
	color: <?= $color->light; ?>;
}
a:link, a:visited {
	color: #3e3e3e;
	text-decoration: none;
	-webkit-transition: all .2s linear;
	-moz-transition: all .2s linear;
	transition: all .2s linear;
	padding: 0px 3px;
}
a:hover, a:focus, a:active {
	background-color: <?= $color->light; ?>;
	color: white;
}
html,body {
	width: 100%;
	height: 100%;
	background: url(js_bg_html.png) repeat;
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	font-family: 'Droid Sans', sans-serif, arial;
	color: #4E4F56;
}
html {
	border-top: solid 4px <?= $color->light; ?>;
}
#body_wrapper {
	width: 90%;
	margin: 0px 5%;
}
.js_logo {
	margin: 40px 0px;
}
.js_main_container {
	font-size: 18px;
}
h1 {
	font-family: 'Droid Serif', serif;
	display: block;
	font-size: 1.8em;
	line-height: 1em;
	font-weight: normal;
	max-width: 700px;
	
	margin: 10px 0px;
}
.js_description {
	line-height: 1.5em;
	max-width: 700px;
	
	margin: 40px 0px;
}
.js_contact_container {
	margin: 0px 0px 40px;
	font-size: 13px;
}
.js_contact_container div {
	display: inline-block;
}
.js_contact_form {
	vertical-align: top;
	margin: 15px 0px;
	vertical-align: top;
	position: relative;
}
.js_contact_form_comment {
	font-size: 0.9em;
	display: block;
	text-decoration: underline;
}
#js_email_input {
	vertical-align: top;
	width: 240px;
	height: 50px;
	margin: 15px 0px;
	background: white;
	border: 2px #CCC;
	box-shadow: 1px 1px 0 0 rgba(0, 0, 0, 0.1) inset, 1px 1px 0 0 rgba(0, 0, 0, 0.15);
	-webkit-appearance: none;
	font-family: 'Droid Sans', sans-serif;
	font-weight: 300;
	font-size: 14px;
	color: #bbbbbb;
	font-style: italic;
	text-align: left;
	line-height: 26px;
	padding-left: 15px;
	padding-right: 35px;
	outline: none;
}
#js_email_input:focus {
	-webkit-box-shadow: 0px 0px 6px #999999; 
	-moz-box-shadow: 0px 0px 6px #999999; 
	box-shadow: 0px 0px 6px #999999; 
}
#js_email_input:focus,
#js_email_input.filled {
	color: #333333;
	font-size: 16px;
	font-style: normal;
}
#js_email_submit {
	display: inline-block;
	vertical-align: top;
	height: 50px;
	margin: 15px 0px;
	margin-right: 50px;
	box-shadow: 1px 1px 0 0 rgba(0, 0, 0, 0.1) inset, 0px 1px 0 0 rgba(0, 0, 0, 0.15);
	-webkit-appearance: none;
	font-family: 'Droid Sans', sans-serif;
	font-size: 14px;
	line-height: 26px;
	padding: 0px 20px;
	outline: none;
	background-color: <?= $btn['main'] ?>;
	background-image: -webkit-gradient(linear, left top, left bottom, from(<?= $btn['light'] ?>), to(<?= $btn['main'] ?>));
	background-image: -webkit-linear-gradient(top, <?= $btn['light'] ?>, <?= $btn['main'] ?>);
	background-image: -moz-linear-gradient(top, <?= $btn['light'] ?>, <?= $btn['main'] ?>);
	background-image: -o-linear-gradient(top, <?= $btn['light'] ?>, <?= $btn['main'] ?>);
	background-image: -ms-linear-gradient(top, <?= $btn['light'] ?>, <?= $btn['main'] ?>);
	background-image: linear-gradient(top, <?= $btn['light'] ?>, <?= $btn['main'] ?>);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='<?= $btn['light_hex'] ?>', EndColorStr='<?= $btn['main_hex'] ?>');
	border: 1px solid <?= $btn['dark'] ?>;
	color: white;
	text-decoration: none;
	text-shadow: 0 1px 0 #00002D;	
	cursor: pointer;
}
#js_email_submit:hover, #js_email_submit:focus {
	background-color: <?= $btn['light'] ?>;
	background-image: -webkit-gradient(linear, left top, left bottom, from(<?= $btn['lighter'] ?>), to(<?= $btn['dark'] ?>));
	background-image: -webkit-linear-gradient(top, <?= $btn['lighter'] ?>, <?= $btn['dark'] ?>);
	background-image: -moz-linear-gradient(top, <?= $btn['lighter'] ?>, <?= $btn['dark'] ?>);
	background-image: -o-linear-gradient(top, <?= $btn['lighter'] ?>, <?= $btn['dark'] ?>);
	background-image: -ms-linear-gradient(top, <?= $btn['lighter'] ?>, <?= $btn['dark'] ?>);
	background-image: linear-gradient(top, <?= $btn['lighter'] ?>, <?= $btn['dark'] ?>);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='<?= $btn['lighter_hex'] ?>', EndColorStr='<?= $btn['dark_hex'] ?>');
	border: 1px solid <?= $btn['darker'] ?>;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3);
	color: white;
	text-shadow: 0 1px 0 #00002D;
}

#js_email_submit:active {
	filter: none;
	background: <?= $btn['main'] ?>;
	border: 1px solid <?= $btn['darker'] ?>;
	box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3);
}

#js_email_reset {
	display: inline-block;
	vertical-align: middle;
	width: 20px;
	height: 20px;
	background: url(js_icon_input_reset.png) no-repeat;
	border: none;
	position: absolute;
	top: 30px;
	left: 260px;
	cursor: pointer;
}

#js_email_reset:hover {
	background: url(js_icon_input_reset.png) no-repeat -40px 0px;
}

.js_contact_info {
	line-height: 1.7em;
	padding: 10px 0px;
	padding-left: 50px;
	border-left: dashed 1px #bbbbbb;
	margin: 15px 0px;
}
#map-side-bar {
	margin: 10px 0px;
}
.js_map_container {
	height: 300px;
	width: 100%;
	display: inline-block;
	background: #cccccc;
	border: solid 1px #ddd; 
}
.js_footer_container {
	height: 50px;
	line-height: 50px;
	font-size: 0.75em;
	
	margin-left: 0px;
	margin-right: 0px;
}
.js_footer_container a:hover {
	background: none;
}
.ui-tooltip, .qtip {
	min-width: 250px;
	max-width: 800px;
}
.js_tooltip .ui-tooltip-content {
	min-width: 250px;
	text-align: center;
}
#qtip-overlay {
	z-index: 300;
}
.js_tooltip_ajax {
	border-bottom: dashed 1px #333333;
}
.js_tooltip_ajax:hover {
	border-bottom: none;
}
.ui-tooltip-light.js-ajax-tooltip .ui-tooltip-content {
	font-size: 1.2em;
	line-height: 1.4em;
	padding: 15px 30px;	
	background: white;
	border: solid 3px #bbbbbb;
}
.js-validation-tooltip {
	min-width: 290px;
	max-width: 300px;
}
.ui-tooltip-light.js-validation-tooltip .ui-tooltip-content {
	font-size: 1.2em;
	line-height: 30px;
	padding: 2px 10px;	
	background: white;
	border: solid 1px #d6d6d6;
	text-align: center;
}
.ui-tooltip-light.js-validation-tooltip .ui-tooltip-content img {
	margin-right: 0px;
	margin-top: 2px;
	margin-bottom: -6px;
	float:left;
}
.js-validation-tooltip {
	-webkit-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.15);
	-moz-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.15);
	box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.15);
}

	.js-validation-tooltip .ui-tooltip-titlebar,
	.js-validation-tooltip .ui-tooltip-content{
		filter: progid:DXImageTransform.Microsoft.Shadow(Color='gray', Direction=90, Strength=3);
		-ms-filter:"progid:DXImageTransform.Microsoft.Shadow(Color='gray', Direction=90, Strength=3)";

		_margin-bottom: -3px; /* IE6 */
		.margin-bottom: -3px; /* IE7 */
	}

a.selected_tooltip {
	color: #3e3e3e;
	text-decoration: none;
	-webkit-transition: all .2s linear;
	-moz-transition: all .2s linear;
	transition: all .2s linear;
	padding: 2px 5px;
	margin-left: -2px;
	margin-top: -1px;
	background-color: <?= $color->light; ?>;
	color: white;
	position: absolute;
	z-index: 301;
	border-bottom: none;
}

.js_normal {
	display: inline-block;
}
.js_contact_form_comment {
	font-size: 0.9em;
	display: block;
}
.js_mobile {
	display: none;
}

.js_message
{
		-webkit-background-size: 40px 40px;
		-moz-background-size: 40px 40px;
		background-size: 40px 40px;			
		background-image: -webkit-gradient(linear, left top, right bottom,
								color-stop(.25, rgba(255, 255, 255, .05)), color-stop(.25, transparent),
								color-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .05)),
								color-stop(.75, rgba(255, 255, 255, .05)), color-stop(.75, transparent),
								to(transparent));
		background-image: -webkit-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%,
							transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%,
							transparent 75%, transparent);
		background-image: -moz-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%,
							transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%,
							transparent 75%, transparent);
		background-image: -ms-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%,
							transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%,
							transparent 75%, transparent);
		background-image: -o-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%,
							transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%,
							transparent 75%, transparent);
		background-image: linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%,
							transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%,
							transparent 75%, transparent);
								
		 -moz-box-shadow: inset 0 -1px 0 rgba(255,255,255,.4);
		 -webkit-box-shadow: inset 0 -1px 0 rgba(255,255,255,.4);		
		 box-shadow: inset 0 -1px 0 rgba(255,255,255,.4);
		 width: 100%;
		 border: 1px solid;
		 color: #fff;
		 padding: 8px 15px;
		 position: fixed;
		 _position: absolute;
		 text-shadow: 0 1px 0 rgba(0,0,0,.5);
		 -webkit-animation: animate-bg 5s linear infinite;
		 -moz-animation: animate-bg 5s linear infinite;
		 text-align: center;
}

.info
{
		 background-color: #4ea5cd;
		 border-color: #3b8eb5;
}

.error
{
		 background-color: #de4343;
		 border-color: #c43d3d;
}
		 
.warning
{
		 background-color: #eaaf51;
		 border-color: #d99a36;
}

.success
{
		 background-color: #61b832;
		 border-color: #55a12c;
}

@-webkit-keyframes animate-bg
{
    from {
        background-position: 0 0;
    }
    to {
       background-position: -80px 0;
    }
}


@-moz-keyframes animate-bg 
{
    from {
        background-position: 0 0;
    }
    to {
       background-position: -80px 0;
    }
}


#trigger-list
{
		 text-align: center;
		 margin: 100px 0;
		 padding: 0;
}

#trigger-list li
{
		 display: inline-block;
		 *display: inline;
		 zoom: 1;
}

#trigger-list .trigger
{
		 display: inline-block;
		 background: #ddd;
		 border: 1px solid #777;
		 padding: 10px 20px;
		 margin: 0 5px;
		 font: bold 12px Arial, Helvetica;
		 text-decoration: none;
		 color: #333;
		 -moz-border-radius: 3px;
		 -webkit-border-radius: 3px;
		 border-radius: 3px;
}

#trigger-list .trigger:hover
{
		background: #f5f5f5;
}